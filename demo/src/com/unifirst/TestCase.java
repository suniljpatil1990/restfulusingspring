package com.unifirst;

public class TestCase {

	public static int result(String[] T,String[] R){
		
		int size=T.length;
		int countSameGroup=-1;
		int result=0;
		for(int i=0;i<size;i++){
			if(Character.isAlphabetic(T[i].charAt(T[i].length()-1))){
				countSameGroup++;
			}
		}
		
	 int totalGroupNo=size-countSameGroup;
	 int numberOfPassedTests=0;
	 for(int i=0;i<size;i++){
		 if(R[i].equals("OK") && !Character.isAlphabetic(T[i].charAt(T[i].length()-1))){
			 numberOfPassedTests++;
		 }
	 }
	 
	return result=(numberOfPassedTests*100)/totalGroupNo; 
	}
	
	public static void main(String[] args) {
		
		String[] T=new String[5];
		String[] R=new String[5];
		
		T[0] = "codility1";   R[0] = "Wrong answer";
		T[1] = "codility3";   R[1] = "OK";
		T[2] = "codility2";   R[2] = "OK";
		T[3] = "codility4b";  R[3] = "Runtime error";
		T[4] = "codility4a";  R[4] = "OK";
		
		System.out.println(result(T, R));
	}
}

