package com.unifirst;

import java.util.BitSet;

public class ReverseArrayElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] input={2,3,4,5,4,0,7,8,9,0,11,12,4,5};
		int[] result=new int[input.length];
		result=reverseElementsOfArray(input);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
	
	public static int[] reverseElementsOfArray(int[] input){
		
		int left=0;
	    int right=input.length-1;
	    BitSet bitSet=new BitSet();
	    
		while(left<right){
			left=input[left]==0?left+1:left;
			right=input[right]==0?right-1:right;
				int temp=input[left];
				input[left]=input[right];
				input[right]=temp;
			
			left++;
			right--;
		
		}
		
		return input;
	}

}
