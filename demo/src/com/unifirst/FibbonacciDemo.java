package com.unifirst;

import java.util.Scanner;

public class FibbonacciDemo {

	public static void main(String[] args) {
		System.out.println("Please enter number:");
		Scanner scanner=new Scanner(System.in);
		 Integer number=scanner.nextInt();
		 for(int i=1;i<number;i++){
			 System.out.println(fibbonacciNumber(i)+" ");
		 }
		 
		 
		 
	}
	
	public static int fibbonacciNumber(int number){
		if(number==1 || number==2){
			return 1;
		}
		
		return fibbonacciNumber(number-1) + fibbonacciNumber(number-2);
	}
}
