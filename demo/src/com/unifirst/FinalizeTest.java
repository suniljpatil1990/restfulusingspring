package com.unifirst;

public class FinalizeTest {

	@Override
	public void finalize(){
		System.out.println("Finalize method called");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FinalizeTest finalizeTest1=new FinalizeTest();
		FinalizeTest finalizeTest2=new FinalizeTest();
		
		finalizeTest1=null;
		finalizeTest2=null;
		
		System.gc();

	}

}
