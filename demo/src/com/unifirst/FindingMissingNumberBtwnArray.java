package com.unifirst;

import java.util.Arrays;
import java.util.BitSet;

public class FindingMissingNumberBtwnArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		printMissingNumber(new int[]{10,22,30,42,60},60);

	}

	private static void printMissingNumber(int[] input, int count) {
		// TODO Auto-generated method stub
		int missingCount=count-input.length;
		BitSet bitSet=new BitSet(count);
		
		for(int number:input){
			bitSet.set(number-1);
			
		}
		System.out.println("Missing number in integer arrays %s, with total number"+Arrays.toString(input));
		int lastMissingIndex=0;
		for(int i=0;i<missingCount;i++){
			lastMissingIndex=bitSet.nextClearBit(lastMissingIndex);
			System.out.println(++lastMissingIndex);
		}
		
	}
}
